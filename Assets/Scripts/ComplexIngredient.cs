﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexIngredient : Ingredient
{
    List<Ingredient> ingredients = new List<Ingredient>();

    public  ComplexIngredient(string name, List<Ingredient> ingredients) : base(name, 0, 0, 0)
    {
        this.ingredients = ingredients;
    }
    public override void Boil()
    {
        Debug.Log("Режем " + Name);
    }

    public override void Cut()
    {
        Debug.Log("Варим " + Name);
    }

    public override void Fry()
    {
        Debug.Log("Жарим " + Name);
    }

    public override float GetTotalCalories()
    {
        float count = 0;
        foreach (Ingredient ingredient in ingredients)
        {
            count += ingredient.GetTotalCalories();
        }
        return count;
    }

    public override float GetTotalCost()
    {
        float count = 0;
        foreach(Ingredient ingredient in ingredients)
        {
            count += ingredient.GetTotalCost();
        }
        return count;
    }

    public override float GetTotalWeight()
    {
        float count = 0;
        foreach (Ingredient ingredient in ingredients)
        {
            count += ingredient.GetTotalWeight();
        }
        return count;
    }
    public override void AddIngredient(Ingredient i)
    {
        this.ingredients.Add(i);
    }
    public override void RemoveIngredient(Ingredient i)
    {
        this.ingredients.Remove(i);
    }
}
