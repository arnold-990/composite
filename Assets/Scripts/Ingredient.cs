﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient 
{
    public string Name;
    public float Wheight;
    public float Kkal;
    public float Price;
    protected Ingredient(string name, float wheight, float kkal, float price)
    {
        Name = name;
        Wheight = wheight;
        Kkal = kkal;
        Price = price;
    }
    public virtual void Cut() { }
    public virtual void Boil() { }
    public virtual void Fry() { }
    public virtual float GetTotalCost()
    {
        return Price * Wheight;
    }
    public virtual float GetTotalWeight()
    {
        return Wheight;
    }
    public virtual float GetTotalCalories()
    {
        return Wheight * Kkal;
    }
    public virtual void AddIngredient(Ingredient i) { }
    public virtual void RemoveIngredient(Ingredient i) { }
}
