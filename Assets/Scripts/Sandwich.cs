﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sandwich : MonoBehaviour
{
    public void Start()
    {
        SimpleIngredient mayo = new SimpleIngredient("майонез", 20, 5, 100);
        SimpleIngredient ketchup = new SimpleIngredient("кетчуп", 30, 10, 100);
        ComplexIngredient ketchenez = new ComplexIngredient("кетченез", new List<Ingredient> { mayo, ketchup });
        SimpleIngredient bread = new SimpleIngredient("хлеб", 80, 15, 300);
        SimpleIngredient cheese = new SimpleIngredient("сыр", 50, 20, 200);
        ComplexIngredient sandwich = new ComplexIngredient("сандвич", new List<Ingredient> { bread, ketchenez, cheese });
        Debug.Log(sandwich.GetTotalCalories());
        Debug.Log(sandwich.GetTotalWeight());
        Debug.Log(sandwich.GetTotalCost());
    }
}
