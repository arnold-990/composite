﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleIngredient : Ingredient
{
    public SimpleIngredient(string name, float wheight, float kkal, float price):base(name,wheight,kkal,price)
    {

    }
    public override void Boil()
    {
        Debug.Log("Режем " + Name);
    }

    public override void Cut()
    {
        Debug.Log("Варим " + Name);
    }

    public override void Fry()
    {
        Debug.Log("Жарим " + Name);
    }

    public override float GetTotalCalories()
    {
        return base.GetTotalCalories();
    }

    public override float GetTotalCost()
    {
        return base.GetTotalCost();
    }

    public override float GetTotalWeight()
    {
        return base.GetTotalWeight();
    }
    public override void AddIngredient(Ingredient i)
    {
        base.AddIngredient(i);
    }
    public override void RemoveIngredient(Ingredient i)
    {
        base.RemoveIngredient(i);
    }
}
